#include <stdio.h>
#include <stdlib.h>

#include <unity.h>
#include <get_urls.h>

/* Unity context functions */
void setUp() {}
void tearDown() {}

FILE * get_data_file(char * data_name) {
  char filepath[1024];

  snprintf(filepath, 1024, "test_get_urls_data/%s.hex", data_name);

  FILE * f = fopen(filepath, "r");

  if(f == NULL) {
    fprintf(stderr, "[get_data_file] Can't open %s", filepath);

    exit(-1);
  }

  return f;
}

void test_one_session_command() {
  FILE * one_session_command_file = get_data_file("01_one_session_command");

  struct SessionCommandArray one_session_command = get_session_commands(one_session_command_file);

  TEST_ASSERT_EQUAL_INT_MESSAGE(1, one_session_command.length, "Wrong Number of Session Commands");

  struct SessionCommand * session_command = one_session_command.array;

  TEST_ASSERT_EQUAL_INT_MESSAGE(2, session_command->size, "Wrong Session Command size");
  TEST_ASSERT_EQUAL_INT_MESSAGE(1, session_command->command_id, "Wrong Session Command ID");

  destroy_session_commands(&one_session_command);
  fclose(one_session_command_file);
}

void test_multiple_session_commands() {
  FILE * multiple_session_commands_file = get_data_file("02_multiple_session_commands");

  struct SessionCommandArray multiple_session_commands = get_session_commands(multiple_session_commands_file);

  TEST_ASSERT_EQUAL_INT(3, multiple_session_commands.length);

   struct SessionCommand
     *session_command_00 = multiple_session_commands.array + 0, 
     *session_command_01 = multiple_session_commands.array + 1,
     *session_command_02 = multiple_session_commands.array + 2;

   TEST_ASSERT_EQUAL_INT(2,session_command_00->size);
   TEST_ASSERT_EQUAL_INT(0,session_command_00->command_id);

   TEST_ASSERT_EQUAL_INT(2,session_command_01->size);
   TEST_ASSERT_EQUAL_INT(0,session_command_01->command_id);   

   TEST_ASSERT_EQUAL_INT(2,session_command_02->size);
   TEST_ASSERT_EQUAL_INT(0,session_command_02->command_id);

   destroy_session_commands(&multiple_session_commands);
   fclose(multiple_session_commands_file);
}

void test_one_url_from_session_command() {
  FILE * one_url_file = get_data_file("03_one_url");

  struct SessionCommandArray one_url_session_commands = get_session_commands(one_url_file);

  TEST_ASSERT_EQUAL_INT(1, one_url_session_commands.length);

  char **urls = get_urls_from_session_commands(one_url_file, one_url_session_commands);
  char *url = urls[0];

  TEST_ASSERT_EQUAL_STRING(url, "TEST");

  char *pending_null = urls[1];

  TEST_ASSERT_NULL(pending_null);

  destroy_urls(urls);
  destroy_session_commands(&one_url_session_commands);
  fclose(one_url_file);
}

int main() {
  UNITY_BEGIN();

  RUN_TEST(test_one_session_command);
  RUN_TEST(test_multiple_session_commands);

  RUN_TEST(test_one_url_from_session_command);

  return UNITY_END();
}
