#pragma once

struct SessionCommand {
  long position;
  uint16_t size;
  uint8_t command_id;
};

struct SessionCommandArray {
  size_t length;
  struct SessionCommand * array;
};


struct SessionCommandArray get_session_commands(FILE * f);
void destroy_session_commands(struct SessionCommandArray *session_commands);
void dprint_session_commands(const struct SessionCommandArray* session_commands);


void print_urls(FILE* f, struct SessionCommandArray session_commands);

char** get_urls_from_session_commands(FILE* f, struct SessionCommandArray session_commands);
void destroy_urls(char ** urls_p);

char* get_url_from_session_command(FILE *f, struct SessionCommand session_command);


