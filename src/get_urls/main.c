#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <errno.h>

#include "get_urls.h"


int main(int argc, char** argv) {

  if(argc == 1) {
    printf("Missing argument\n");

    return EXIT_FAILURE;
  } else {
    const char * filepath = argv[1];

    FILE * f = fopen(filepath, "r");
    
    if(f != NULL) {
      struct SessionCommandArray session_commands = get_session_commands(f);

      dprint_session_commands(&session_commands);

      print_urls(f, session_commands);

      destroy_session_commands(&session_commands);
      fclose(f);
      return EXIT_SUCCESS;
    } else {

      printf("Can't open the file %s\n", filepath);

      return EXIT_FAILURE;
    }

  }
}
