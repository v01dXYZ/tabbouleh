#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include <inttypes.h>

#include "get_urls.h"

struct SessionCommandArray get_session_commands(FILE * f) {
  const size_t max_elements = 1024;

  struct SessionCommand * array = calloc(max_elements,
					    sizeof(struct SessionCommand));
  
  const long header_offset = 8L;
  fseek(f, header_offset, SEEK_CUR);

  uint16_t size = 1;
  int counter = 1;
  int status = 0;

  size_t i;
  for(i=0;
         i < max_elements
      && counter != 0
      && size != 0
      && status == 0;
      i+=1) {
    uint8_t command_id;
    long position;
    
    counter = fread(&size, sizeof(size), 1, f);
    if (counter == 0 ) {
      break;
    }

    position = ftell(f);
    fread(&command_id, sizeof(command_id), 1, f);
    
    struct SessionCommand* session_command = (array + i);
    session_command->position = position; 
    session_command->command_id = command_id;
    session_command->size = size;

    status = fseek(f, size - sizeof(command_id), SEEK_CUR);
  }

  struct SessionCommandArray result = { .length = i,
					.array = array };

  return result;
}


void destroy_session_commands(struct SessionCommandArray *session_commands) {
  free(session_commands->array);
  session_commands->length = 0;
}


void print_urls
(FILE* f,
const struct SessionCommandArray session_commands) {
  char** urls = get_urls_from_session_commands(f, session_commands);

  for(size_t ix=0; urls[ix];ix+=1) {
    const char *url = urls[ix];

    printf("%s\n", url);
  }

  destroy_urls(urls);
}


char ** get_urls_from_session_commands
(FILE* f,
 const struct SessionCommandArray session_commands) {

  // + 1 to be sure there is a pending NULL pointer at the end
  // Overallocation but cheap since 8 bytes only
  char ** urls = calloc(session_commands.length + 1, sizeof(char *));

  size_t ix_url=0;
  for(size_t ix =0; ix<session_commands.length; ix+=1) {
    const struct SessionCommand session_command = session_commands.array[ix];

    if(session_command.command_id == 1) {
      urls[ix_url] = get_url_from_session_command(f, session_command);
      ix_url += 1;
    }
  }

  return urls;
}


char* get_url_from_session_command(FILE *f,
				   const struct SessionCommand session_command) {
    const long offset_command_id_to_string_length = 13L; // starting from the byte *prior* to command id byte

    fseek(f,
	  session_command.position + offset_command_id_to_string_length,
	  SEEK_SET);

    uint32_t string_length;
    fread(&string_length, sizeof(string_length), 1, f);

    fprintf(stderr,
	    "position: %ld, string_length %" PRIu32 "\n",
	    session_command.position,
	    string_length);

    char *url = calloc(string_length + 1, sizeof(char)); // + 1 to get C null-terminated strings
    fread(url, 1, string_length, f);

    return url;
}

void destroy_urls(char ** urls) {

  for(size_t ix=0; urls[ix]; ix+=1) {
    free(urls[ix]);
  }

  free(urls);
}

void dprint_session_commands(const struct SessionCommandArray* session_commands) {

  fprintf(stderr,
	  "[DEBUG] SESSION COMMAND" "\n"
	  "-----------------------" "\n");

  for(size_t i=0; i<session_commands->length; i+=1) {
    const struct SessionCommand session_command = session_commands->array[i];

    fprintf(stderr,
	    "[%3d] "
	    "position: %8ld, size: %5" PRIu16 ", command_id: %2" PRIu8 "\n",
	    i,
	    session_command.position,
	    session_command.size,
	    session_command.command_id);
  }
}
