use std::io::{self, BufRead};

struct HTTPGetRequest {
    host: String,
    ressource: String,
    query: String,
}

enum HTTPGetRequestProtocol {
    HTTP,
    HTTPS
}

fn print_request(request: HTTPGetRequest) {
    println!("Host: {}, Ressource: {}, Query: {}", request.host, request.ressource, request.query);
}

fn get_protocol(line: &str) -> Option<(HTTPGetRequestProtocol, &str)> {
    if line.starts_with("http://") {
	return Some((HTTPGetRequestProtocol::HTTP,
		     &line[7..]));
    }

    if line.starts_with("https://") {
	return Some((HTTPGetRequestProtocol::HTTPS,
		     &line[8..]));
    }

    None
}

fn get_domain_name(line_without_protocol: &str) -> (&str, &str) {
    let domain_name_length = line_without_protocol.find("/");

    match domain_name_length {
	Some(domain_name_length) => (&line_without_protocol[..domain_name_length], &line_without_protocol[domain_name_length..]),
	None => (line_without_protocol, ""),
    }
}

fn get_query(line_without_domain_name: &str) -> (&str, &str) {
    let ressource_length = line_without_domain_name.find("?");

    match ressource_length {
	Some(ressource_length) => (&line_without_domain_name[..ressource_length], &line_without_domain_name[ressource_length..]),
	None => (line_without_domain_name, ""),
    }
}

fn get_http_get_request_from_line(line: &str) -> Option<HTTPGetRequest>{
    let (protocol, line_without_protocol) = get_protocol(line)?;
    let (domain_name, line_without_domain_name) = get_domain_name(line_without_protocol);
    let (ressource, query) = get_query(line_without_domain_name);

    Some(HTTPGetRequest {
	host: String::from(domain_name),
	ressource: String::from(ressource),
	query: String::from(query),
    })
}

fn main() ->  io::Result<()>{
    let stdin = io::stdin();
    
    for line in stdin.lock().lines() {
	let request = get_http_get_request_from_line(&line?);

	match request {
	    Some(request) => {
		print_request(request);
	    }
	    None => {
		println!("Ignoring line");
	    }
	}
    }

    Ok(())
}
