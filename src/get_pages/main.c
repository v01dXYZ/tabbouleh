#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>

// 3rd libraries
#include <civetweb.h>

enum HTTPGetRequest_protocol {
			      HTTPGetRequest_protocol_HTTP,
			      HTTPGetRequest_protocol_HTTPS,
			      HTTPGetRequest_protocol_NUMBER,
};

struct HTTPGetRequest {
  char* host;
  char* ressource;
  char* query;
  enum HTTPGetRequest_protocol protocol;
};

struct HTTPGetRequest* construct_HTTPGetRequest
(char* host,
 char* ressource,
 char* query,
 enum HTTPGetRequest_protocol protocol) {
  struct HTTPGetRequest *request = malloc(sizeof(struct HTTPGetRequest));

  request->host = host;
  request->ressource = ressource;
  request->query = query;
  request->protocol = protocol;

  return request;
}

void destroy_HTTPGetRequest(struct HTTPGetRequest* request) {
  free(request->host);
  free(request->ressource);
  free(request->query);

  free(request);
}

struct HTTPGetRequest* get_HTTP_GET_request_from_line(char* line, size_t line_length) {
  size_t host_offset;

  enum HTTPGetRequest_protocol protocol;

  // not efficient but who cares at this stage
  const char *http_str = "http://";
  size_t http_str_len = sizeof(http_str) - 1;
  bool protocol_is_http = (strncmp(http_str, line, http_str_len) == 0);

  if(protocol_is_http) {
    host_offset = sizeof(http_str);
    protocol = HTTPGetRequest_protocol_HTTP;
  }

  const char *https_str = "https://";
  size_t https_str_len = sizeof(https_str) - 1;
  bool protocol_is_https = (strncmp(https_str, line, https_str_len) == 0);

  if(protocol_is_https) {
    host_offset = sizeof(https_str);
    protocol = HTTPGetRequest_protocol_HTTPS;
  }

  if(!protocol_is_http && !protocol_is_https) {
    fprintf(stderr, "[Ignore] protocol is neither http nor https\n");
    return NULL;
  }

  char *host_start = line + host_offset;

  const char *host_end = strchr(host_start, '/');
  const size_t host_length = host_end ? (host_end  - host_start) : line_length;

  const char *query_start = host_end ? strchr(host_end, '?') : NULL;

  const size_t ressource_length = query_start ? query_start - host_end : line_length;

  char *host = strndup(host_start, host_length);
  char *ressource = host_end ? strndup(host_end, ressource_length): NULL;
  char *query = query_start ? strdup(query_start) : NULL;

  struct HTTPGetRequest *request = construct_HTTPGetRequest(host, ressource, query, protocol);

  return request;
}

void destroy_requests(struct HTTPGetRequest** requests) {
  for(struct HTTPGetRequest **request_p = requests; *request_p; request_p +=1) {
      destroy_HTTPGetRequest(*request_p);
  }

  free(requests);
}

char* get_content(struct HTTPGetRequest* request) {
  int protocol_to_port[HTTPGetRequest_protocol_NUMBER] = {
			     [HTTPGetRequest_protocol_HTTP] = 80,
			     [HTTPGetRequest_protocol_HTTPS] = 443
  };

  int port = protocol_to_port[request->protocol];
  bool is_with_ssl_layer = (request->protocol == HTTPGetRequest_protocol_HTTPS);

  const size_t error_msg_max_length = 1 << 11;
  char error_msg[error_msg_max_length];

  struct mg_connection *con = mg_connect_client(request->host, port, is_with_ssl_layer, error_msg, error_msg_max_length);

  char *buffer = NULL;

  if(con) {
    char * ressource = request->ressource ? request->ressource : "/";

    mg_printf(con,
	      "GET %s HTTP/1.1"		"\r\n"
	      "Host: %s"		"\r\n"
	      "\r\n",
	      ressource,
	      request->host);

    mg_get_response(con, NULL, 0, 5000); // 5s timeout

    // returns a pointer to the inner state of the connetion
    // no need to free it
    struct mg_response_info *resp = mg_get_response_info(con);

    ssize_t buffer_length = resp->content_length;

    printf("content-length: %d\n", resp->content_length);

    if(buffer_length > 0) {
      buffer = malloc(buffer_length);
      mg_read(con, buffer, buffer_length);
      puts(buffer);
    }

    mg_close_connection(con);
  } else {
    fprintf(stderr, "[Connection Error] %s with host: %s, port: %d\n", error_msg, request->host, port);
  }

  return buffer;
}

void print_all_requests_content(struct HTTPGetRequest** requests) {
  for(struct HTTPGetRequest **request_p = requests; *request_p; request_p+=1) {
    get_content(*request_p);
  }
}

void print_request(struct HTTPGetRequest* request) {
  printf(">> [Request] Host: %s, Ressource: %s, Query: %s\n", request->host, request->ressource, request->query);
}

void print_requests(struct HTTPGetRequest** requests) {
  for(struct HTTPGetRequest **request_p = requests; *request_p; request_p+=1) {
    print_request(*request_p);
  }
}

int main() {
  size_t line_length = 2028;
  char *line = calloc(line_length, sizeof(char));
  ssize_t char_counter = 0;

  const size_t requests_max_length = 1024;
  struct HTTPGetRequest **requests = calloc(requests_max_length + 1, sizeof(struct HTTPGetRequest*));

  size_t i =0;
  while((char_counter = getline(&line, &line_length, stdin)) >= 0
	&& i<requests_max_length){
    line[char_counter - 1] = 0; // remove the trailing newline char ONLY for Linux unfortunately

    struct HTTPGetRequest *request = get_HTTP_GET_request_from_line(line, line_length);

    if(!request) {
      continue;
    }

    requests[i] = request;
    i+=1;
  }

  print_requests(requests);
  //  print_all_requests_content(requests);

  destroy_requests(requests);
  free(line);

  return EXIT_SUCCESS;
}
