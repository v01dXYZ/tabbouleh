#[macro_use]
extern crate fstrings;

use std::fs::File;
use seek_bufread::{BufReader};
use std::io::{Read, Seek, SeekFrom};
use byteorder::{ReadBytesExt, NativeEndian};

struct SNSSHeader {
    name: [u8; 4],
    version: u32,
}

struct SessionCommand {
    position: u64,
    size: u16,
    command_id: u8,
}

struct SessionCommandReader<'a> {
    reader: &'a mut BufReader<File>
}

impl SessionCommandReader<'_> {
    fn new(reader: &mut BufReader<File>) -> SessionCommandReader {
	SessionCommandReader {
	    reader: reader,
	}
    }
}

impl Iterator for SessionCommandReader<'_> {
    type Item = SessionCommand;

    fn next(&mut self) -> Option<SessionCommand> {
	let mut next_with_error = || -> std::io::Result<SessionCommand> {
	    let size = self.reader.read_u16::<NativeEndian>()?;

	    // We consider the size of the Session Command are not part of it
	    let position = self.reader.position();

	    let mut command_id = [0_u8; 1];
	    self.reader.read_exact(&mut command_id);

	    self.reader.seek(SeekFrom::Current((size -1) as i64))?;

	    Ok(SessionCommand {
		position: position,
		size: size,
		command_id: command_id[0],
	    })
	};
	
	next_with_error().ok()
    }
}

fn get_header(reader: &mut BufReader<File>) -> std::io::Result<SNSSHeader> {
    let mut name = [0_u8; 4];
    reader.read_exact(&mut name);

    let version = reader.read_u32::<NativeEndian>()?;

    Ok(SNSSHeader {
	name: name,
	version: version,
    })
}

fn get_session_commands(reader: &mut BufReader<File>) -> std::io::Result<Vec<SessionCommand>> {
    let header = get_header(reader)?;

    let session_command_reader = SessionCommandReader::new(reader);
    
    Ok(session_command_reader.collect())
}

fn eprint_session_commands(session_commands: &Vec<SessionCommand>) {
    eprintln!("[DEBUG] SESSION COMMANDS");
    eprintln!("------------------------");
    
    for (i, session_command) in session_commands.iter().enumerate() {
	eprintln_f!("[{i:3}] position: {session_command.position:8}, size: {session_command.size:5}, command_id: {session_command.command_id:2}");
    }
}

fn print_urls(reader: &mut BufReader<File>, session_commands: &Vec<SessionCommand>) {
    for session_command in session_commands {
	if session_command.command_id != 1 {
	    continue;
	}

	let offset_string_size = 13_u64;
	reader.seek(SeekFrom::Start(session_command.position + offset_string_size));

	let string_size = reader.read_u32::<NativeEndian>().unwrap();
	
	eprintln_f!("position: {session_command.position} string_length: {string_size}");

	let mut string_data = vec!(0_u8; string_size as usize);

	reader.read_exact(&mut string_data);

	let url = String::from_utf8(string_data).unwrap();

	println_f!("{url}");
    }
}

fn main() -> std::io::Result<()> {
    let file = File::open("../../data/Current_Tabs.archive")?;
    let mut reader = BufReader::new(file);

    let session_commands = get_session_commands(&mut reader)?;

    eprint_session_commands(&session_commands);
    print_urls(&mut reader, &session_commands);

    Ok(())
}
